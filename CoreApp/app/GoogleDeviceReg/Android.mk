LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := GoogleDeviceReg
LOCAL_MODULE_CLASS := APPS
LOCAL_PROGUARD_ENABLED := disabled
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := GoogleDeviceReg.apk
LOCAL_CERTIFICATE := platform

include $(BUILD_PREBUILT)
